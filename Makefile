CC=gcc
CFLAGS=-lm -I.
DEPS = point.h
OBJ = main.o point.o 

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS) 

programa: $(OBJ)
	gcc -o $@ $^ $(CFLAGS)

clean:
	rm -f programa *.o
