#include <stdio.h>
#include <math.h>
#include <point.h>

int main(){

	double d;
	double resultado;
	printf("Calcular distancia euclidiana entren dos puntos: \n");

	struct point pnts[2];

	for(int i=1; i<3; i++){
		printf("Punto n°%d: \n",i);
		printf("Ingresar coordenada x: ");
		scanf("%f",&pnts[i].x);
		printf("Ingresar coordenada y: ");
		scanf("%f",&pnts[i].y);
		printf("Ingresar coordenada z: ");
		scanf("%f",&pnts[i].z);
	}
	
	d = distancia(pnts);

	printf("%.2f \n", sqrt(d));

}
